<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'SiteController@home')->name('home');
Route::get('/Sobre', 'SiteController@sobre')->name('sobre');
Route::get('/Planos', 'SiteController@planos')->name('planos');
Route::get('/Assistencia24h', 'SiteController@assistencia')->name('assistencia');
Route::get('/Contato', 'SiteController@contato')->name('contato');
Route::get('/EnviarMensagem', 'SiteController@mensagemPost')->name('enviarmensagem');
Route::get('/Newsletter', 'SiteController@newsletterPost')->name('newsletterPost');

Route::get('/Login', 'AcessoController@login')->name('login');
Route::post('/verifica', 'AcessoController@verifica');
Route::get('/CadastroAdmin', 'AcessoController@cadastro')->name('cadastroADMGet');
Route::post('/CadastroAdmin', 'AcessoController@cadastroPost')->name('cadastroADMPost');

//Usuários
Route::get('/Usuarios', 'UsuarioController@usuarios');
Route::post('/todosUsuarios', 'UsuarioController@todosUsuarios')->name('todosUsuarios');
Route::get('/AdicionarUsuario', 'UsuarioController@add');
Route::post('/AdicionarUsuario', 'UsuarioController@addPost');
Route::get('/EditarUsuario/{id}', 'UsuarioController@edt');
Route::post('/EditarUsuario', 'UsuarioController@edtPost');
//Serviços
Route::get('/Servicos', 'ServicoController@servico');
Route::post('/todosServicos', 'ServicoController@todosServicos')->name('todosServicos');
Route::post('/todosServicosLixeira', 'ServicoController@todosServicosLixeira')->name('todosServicosLixeira');
Route::get('/AdicionarServicos', 'ServicoController@add');
Route::get('/EditarServicos/{id}', 'ServicoController@edt');
Route::get('/ExcluirServicos/{id}', 'ServicoController@excluirPost');
Route::post('/EditarServicos/{id}', 'ServicoController@edtPost');
Route::get('/ServicosLixeira', 'ServicoController@lixo');
Route::post('/AdicionarServicos', 'ServicoController@adicionarServicos');