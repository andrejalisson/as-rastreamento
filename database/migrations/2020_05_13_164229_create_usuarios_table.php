<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id('id_user');
            $table->string('nome_user');
            $table->string('usuario_user')->unique();
            $table->string('email_user')->unique();
            $table->binary('imagem_user');
            $table->string('senha_user');
            $table->softDeletes('deletado_user', 0);
            $table->boolean('status_user')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
