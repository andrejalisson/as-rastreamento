<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Servicos', function (Blueprint $table) {
            $table->id('id_ser');
            $table->string('titulo_ser')->nullable();
            $table->string('descrição_ser')->nullable();
            $table->float('valor_ser')->nullable()->default(0.0);
            $table->dateTime('criado_ser')->nullable();
            $table->softDeletes('deletado_ser', 0);
            $table->boolean('status_ser')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Servicos');
    }
}
