<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcessoController extends Controller{
    public function login(){
        $title = "Login";
        return view('admin.acesso.login')->with(compact( 'title'));
    }

    public function verifica(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orWhere('usuario_user', $request->usuario)->first();
        if($usuario != null){
            if($usuario->status_user != 0){
                if(password_verify($request->senha, $usuario->senha_user )){
                    $request->session()->flash('sucesso', 'Bom trabalho!');
                    $request->session()->put('id', $usuario->id_user);
                    $request->session()->put('nome', $usuario->nome_user);
                    $request->session()->put('imagem', $usuario->imagem_user);
                    $request->session()->put('usuario', $usuario->usuario_user);
                    $request->session()->put('logado', true);
                    return redirect('/Servicos');
                }else{
                    $request->session()->flash('alerta', 'Senha incorreta!');
                    return redirect()->back();
                }
            }else{
                $request->session()->flash('alerta', 'Usuário Bloqueado.');
            return redirect()->back();
            }
            
        }else{
            $request->session()->flash('alerta', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function cadastro(){
        $title = "Cadastro Administradores";
        return view('admin.acesso.cadastro')->with(compact( 'title'));
    }

    public function cadastroPost(Request $request){
        $usuario = DB::table('usuarios')->where('usuario_user', $request->usuario)->first();
        if($usuario == null){
            $request->session()->flash('alerta', 'Usuário não disponível.');
            return redirect()->back();
        }

        $usuario = DB::table('usuarios')->where('email_user', $request->email)->first();
        if($usuario == null){
            $request->session()->flash('alerta', 'Email já cadastrado.');
            return redirect()->back();
        }

        DB::table('usuarios')->insert([
            'nome_user' => $request->nome,
            'usuario_user' => $request->nome,
            'email_user' => $request->nome,
            'senha_user' => $request->nome,
            'status_user' => 1,
            ]
        );
        $request->session()->flash('sucesso', 'Usuário Cadastrado com Sucesso!');
        return redirect('/');
    }

}
