@extends('templates.acessoAdmin')

@section('css')
@endsection

@section('corpo')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">A.S</h1>
        </div>
        <h3>A.S Rastreamento</h3>
        <p>Sistema Gerencial</p>
        <p>Faça login, para vê-lo em ação.</p>
        <form class="m-t" role="form" method="POST" action="/verifica" autocomplete="off">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" class="form-control" autofocus name="usuario" placeholder="Usuário" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="senha" placeholder="Senha" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Entrar</button>

            <a href="#"><small>Esqueceu a Senha?</small></a>
        </form>
        <p class="m-t">
            <small>A.S Rastreamento CNPJ: 00.000.000/0001-00 </br> Todos os direitos reservados &copy; {{date('Y')}}</small>
        </p>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('script')
@endsection
