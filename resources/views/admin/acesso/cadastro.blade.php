@extends('templates.acessoAdmin')

@section('css')
@endsection

@section('corpo')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">A.S</h1>
        </div>
        <h3>Cadastro Administrativo</h3>
        <p>Crie uma conta para vê-la em ação.</p>
        <form class="m-t" role="form" method="POST" action="/CadastroAdmin">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nome" name="nome" required="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Usuário" name="usuario" required="">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="email" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Senha" name="senha" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Cadastrar</button>
        </form>
        <p class="m-t">
            <small>A.S Rastreamento CNPJ: 00.000.000/0001-00 </br> Todos os direitos reservados &copy; {{date('Y')}}</small>
        </p>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('script')
@endsection
