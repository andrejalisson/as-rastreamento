<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{$title}} | A.S Rastreamento</title>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="/css/animate.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        @yield('css')
    </head>

<body class="canvas-menu light-skin">

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <a class="close-canvas-menu"><i class="fa fa-times"></i></a>
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" width="48" height="48" class="rounded-circle" src="{{ session('imagem') }}"/>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold">{{ session('usuario') }} <b class="caret"></b></span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="#">Perfil</a></li>
                            <li><a class="dropdown-item" href="#">Mensagens</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">Sair</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        A.S
                    </div>
                </li>
                <li @switch($title)
                    @case("Serviços")
                        class="active"
                        @break
                    @case("Adicionar Serviços")
                        class="active"
                        @break
                    @case("Lixeira de Serviços")
                        class="active"
                        @break
                    @default
                @endswitch
                >
                    <a href="#"><i class="fa fa-empire"></i> <span class="nav-label">Serviços</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li
                        @if($title == "Serviços")
                            class="active"
                        @endif
                        ><a href="/Servicos">Lista</a></li>
                        <li
                        @if($title == "Adicionar Serviços")
                            class="active"
                        @endif
                        ><a href="/AdicionarServicos">Cadastrar</a></li>
                        <li
                        @if($title == "Lixeira de Serviços")
                            class="active"
                        @endif
                        ><a href="/ServicosLixeira">Lixeira</a></li>
                    </ul>
                </li>

                <li @switch($title)
                @case("Usuários")
                    class="active"
                    @break
                @case("Adicionar Usuário")
                    class="active"
                    @break
                @default
            @endswitch
            >
                <a href="#"><i class="fa fa-user-o"></i> <span class="nav-label">Usuários</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li
                    @if($title == "Usuários")
                        class="active"
                    @endif
                    ><a href="/Usuarios">Lista</a></li>
                    <li
                    @if($title == "Adicionar Usuário")
                        class="active"
                    @endif
                    ><a href="/AdicionarUsuario">Cadastrar</a></li>
                </ul>
            </li>


                {{-- <li @switch($title)
                    @case("Movimentação Financeira")
                        class="active"
                        @break
                    @case("Gerenciamento de Cobranças")
                        class="active"
                        @break
                    @case("Manutenção de convênio bancário")
                        class="active"
                        @break
                    @case("Gerenciamento de Comissões")
                        class="active"
                        @break
                    @case("Conta Corrente")
                        class="active"
                        @break
                    @case("Centro de Receita e Custo")
                        class="active"
                        @break
                    @case("Categoria Financeira")
                        class="active"
                        @break
                    @default
                @endswitch
                >
                    <a href="#"><i class="fa fa-bank"></i> <span class="nav-label">Financeiro</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li
                        @if($title == "Movimentação Financeira")
                            class="active"
                        @endif
                        ><a href="#">Movimentação Financeira</a></li>
                        <li
                        @if($title == "Gerenciamento de Cobranças")
                            class="active"
                        @endif
                        ><a href="#">Cobranças</a></li>
                        <li
                        @if($title == "Manutenção de convênio bancário")
                            class="active"
                        @endif
                        ><a href="#">Convênios Bancários</a></li>
                        <li
                        @if($title == "Gerenciamento de Comissões")
                            class="active"
                        @endif
                        ><a href="#">Comissões</a></li>
                        <li
                        @if($title == "Conta Corrente")
                            class="active"
                        @endif
                        ><a href="#">Conta Corrente</a></li>
                        <li
                        @if($title == "Centro de Receita e Custo")
                            class="active"
                        @endif
                        ><a href="#">Receita e Custo</a></li>
                        <li
                        @if($title == "Categoria Financeira")
                            class="active"
                        @endif
                        ><a href="#">Categoria Financeira</a></li>
                    </ul>
                </li> --}}





            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Bem vindo a A.S Rastreamento ERP</span>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">

                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html" class="dropdown-item">
                                    <i class="fa fa-envelope"></i> <strong>Uau, você não tem mensagens a serem lidas.</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html" class="dropdown-item">
                                    <strong>Nenhuma notificação aqui! Parabéns.</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Sair
                    </a>
                </li>
            </ul>

        </nav>
        </div>
        @yield('corpo')
            <div class="footer">
                <div>
                    <strong>A.S Rastreamento</strong> Todos os Direitos Reservados &copy; {{date('Y')}}
                </div>
            </div>

        </div>
        </div>

    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>
    @yield('js')
    <script src="/js/inspinia.js"></script>
    <script src="/js/plugins/pace/pace.min.js"></script>
    @yield('script')
    @if(session('sucesso'))
        <script>
            swal({
                title: "Sucesso!",
                text: "{{session('sucesso')}}",
                type: "success"
            });
        </script>
    @endif
    @if(session('alerta'))
        <script>
            swal({
                title: "Erro!",
                text: "{{session('alerta')}}",
                type: "error"
            });
        </script>
    @endif
    

</body>

</html>

