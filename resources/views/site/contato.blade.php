@extends('templates.site')

@section('css')
@endsection

@section('corpo')

<section class="contact-info-style-one">
    <div class="container">
        <div class="title-block text-center">
            <span class="tag-line">Fale Conosco</span><!-- /.tag-line -->
            <h2>Horário de funcionamento </h2>
                Administrativo de segunda a sexta feira das 8:00 as 18:00 </br>
                Central de monitoramento 24 horas
        </div><!-- /.title-block -->
        <div class="row high-gutter">
            <div class="col-lg-4">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>
                        <h3>Endereço</h3>
                        <p>Rua Teofredo Goiana, 1501 <br> Cidade dos Funcionários</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-smartphone"></i>
                        <h3>Contato</h3>
                        <p>(85) 9 8834-7252</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-opened-email-outlined-interface-symbol"></i>
                        <h3>E-mail</h3>
                        <p>contato@asrastreamento.com.br</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.contact-info-style-one -->

<section class="contact-form-wrapper">
    <div class="container">
        <div class="inner-container">
            <div class="row no-gutters">
                <div class="col-lg-12 d-flex">
                    <div class="contact-form-block my-auto">
                        <div class="title-block">
                            <span class="tag-line">Escreva sua Mensagem</span><!-- /.tag-line -->
                            <h2>Entre em contato</h2>
                        </div><!-- /.title-block -->
                        <form action="/EnviarMensagem" method="POST" class="contact-form-one contact-form-validated">
                            {!! csrf_field() !!}
                            <input type="text" name="nome" placeholder="Seu Nome">
                            <input type="email" name="email" placeholder="E-mail">
                            <input type="tel" name="telefone" placeholder="Telefone">
                            <textarea placeholder="Sua Mensagem" name="mensagem"></textarea>
                            <button type="submit">Enviar agora.</button>
                        </form>
                    </div><!-- /.contact-form-block -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
        </div><!-- /.inner-container -->
    </div><!-- /.container -->
</section><!-- /.contact-form-wrapper -->

@endsection

@section('js')
<script src="/site/js/jquery.validate.min.js"></script>
<script src="/site/js/theme.js"></script>
@endsection

@section('script')
@endsection
